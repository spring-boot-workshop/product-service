package me.bleidner.spring.model;

import lombok.Data;
import lombok.NoArgsConstructor;


@Data
@NoArgsConstructor
public class Product {

    private long id;

    private String name;
    private double price;

    public Product(String name, double price) {

        this.name = name;
        this.price = price;
    }
}
